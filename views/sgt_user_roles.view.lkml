view: sgt_user_roles {
  sql_table_name: dbo.sgt_user_roles ;;

  dimension: created_at {

    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {

    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: role_id {
    hidden: yes
    type: number
    sql: ${TABLE}.roleId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: user_id {
    hidden: yes
    type: number
    sql: ${TABLE}.userId ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}
