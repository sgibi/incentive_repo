view: sgm_dealerships {
  sql_table_name: dbo.sgm_dealerships ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: current_flag {
    type: string
    sql: ${TABLE}."current flag" ;;
  }

  dimension_group: dealer_geography_end {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."dealer geography end date" ;;
  }

  dimension_group: dealer_geography_start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."dealer geography start date" ;;
  }

  dimension: dealer_manager_name {
    type: string
    sql: ${TABLE}."dealer manager name" ;;
  }

  dimension: geography_level_1_code {
    type: string
    sql: ${TABLE}."geography level 1 code" ;;
  }

  dimension: geography_level_1_manager {
    type: string
    sql: ${TABLE}."geography level 1 manager" ;;
  }

  dimension: geography_level_1_name {
    type: string
    sql: ${TABLE}."geography level 1 name" ;;
  }

  dimension: geography_level_1_number {
    type: string
    sql: ${TABLE}."geography level 1 number" ;;
  }

  dimension: geography_level_1_type_identifier {
    type: number
    sql: ${TABLE}."Geography Level 1 Type Identifier" ;;
  }

  dimension: geography_level_2_code {
    type: string
    sql: ${TABLE}."geography level 2 code" ;;
  }

  dimension: geography_level_2_manager {
    type: string
    sql: ${TABLE}."geography level 2 manager" ;;
  }

  dimension: geography_level_2_name {
    type: string
    sql: ${TABLE}."geography level 2 name" ;;
  }

  dimension: geography_level_2_number {
    type: string
    sql: ${TABLE}."geography level 2 number" ;;
  }

  dimension: geography_level_2_type_identifier {
    type: number
    sql: ${TABLE}."Geography Level 2 Type Identifier" ;;
  }

  dimension: geography_level_3_code {
    type: string
    sql: ${TABLE}."geography level 3 code" ;;
  }

  dimension: geography_level_3_manager {
    type: string
    sql: ${TABLE}."geography level 3 manager" ;;
  }

  dimension: geography_level_3_name {
    type: string
    sql: ${TABLE}."geography level 3 name" ;;
  }

  dimension: geography_level_3_number {
    type: string
    sql: ${TABLE}."geography level 3 number" ;;
  }

  dimension: geography_level_3_type_identifier {
    type: number
    sql: ${TABLE}."Geography Level 3 Type Identifier" ;;
  }

  dimension: geography_level_4_code {
    type: string
    sql: ${TABLE}."geography level 4 code" ;;
  }

  dimension: geography_level_4_manager {
    type: string
    sql: ${TABLE}."geography level 4 manager" ;;
  }

  dimension: geography_level_4_name {
    type: string
    sql: ${TABLE}."geography level 4 name" ;;
  }

  dimension: geography_level_4_number {
    type: string
    sql: ${TABLE}."geography level 4 number" ;;
  }

  dimension: geography_level_4_type_identifier {
    type: number
    sql: ${TABLE}."Geography Level 4 Type Identifier" ;;
  }

  dimension: is_active {
    type: string
    sql: ${TABLE}.isActive ;;
  }

  dimension: is_deleted {
    type: string
    sql: ${TABLE}.isDeleted ;;
  }

  dimension: owner_code {
    type: string
    sql: ${TABLE}."owner code" ;;
  }

  dimension: owner_identifier {
    type: number
    sql: ${TABLE}."owner identifier" ;;
  }

  dimension: sales_owner_code {
    type: string
    sql: ${TABLE}."sales owner code" ;;
  }

  dimension: sg_dlr_add1 {
    type: string
    sql: ${TABLE}.sg_dlr_add1 ;;
  }

  dimension: sg_dlr_add2 {
    type: string
    sql: ${TABLE}.sg_dlr_add2 ;;
  }

  dimension: sg_dlr_agent {
    type: string
    sql: ${TABLE}.sg_dlr_agent ;;
  }

  dimension: sg_dlr_city {
    type: string
    sql: ${TABLE}.sg_dlr_city ;;
  }

  dimension: sg_dlr_company {
    type: string
    sql: ${TABLE}.sg_dlr_company ;;
  }

  dimension: sg_dlr_dealer {
    type: string
    sql: ${TABLE}.sg_dlr_dealer ;;
  }

  dimension_group: sg_dlr_edate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sg_dlr_edate ;;
  }

  dimension: sg_dlr_fax {
    type: string
    sql: ${TABLE}.sg_dlr_fax ;;
  }

  dimension_group: sg_dlr_outofbus {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sg_dlr_outofbus ;;
  }

  dimension: sg_dlr_phone {
    type: string
    sql: ${TABLE}.sg_dlr_phone ;;
  }

  dimension_group: sg_dlr_sdate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sg_dlr_sdate ;;
  }

  dimension: sg_dlr_state {
    type: string
    sql: ${TABLE}.sg_dlr_state ;;
  }

  dimension: sg_dlr_zip {
    type: string
    sql: ${TABLE}.sg_dlr_zip ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      dealer_manager_name,
      geography_level_1_name,
      geography_level_2_name,
      geography_level_3_name,
      geography_level_4_name,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name,
      sgm_program_allocations.count,
      sgt_dealership_gms.count,
      sgt_dealership_payments.count,
      sgt_dealership_w9s.count,
      sgt_program_users.count
    ]
  }
}
