view: sgt_dealership_vol {
  sql_table_name: dbo.sgt_dealership_vol ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: multi_cov_vol {
    type: number
    sql: ${TABLE}.multi_cov_vol ;;
  }

  dimension: pen {
    type: number
    sql: ${TABLE}.pen ;;
  }

  dimension: sg_con_dealer {
    type: string
    sql: ${TABLE}.sg_con_dealer ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: sgm_period_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmPeriodId ;;
  }

  dimension: single_cov_vol {
    type: number
    sql: ${TABLE}.single_cov_vol ;;
  }

  dimension: total_vehicle_sale {
    type: number
    sql: ${TABLE}.total_vehicle_sale ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: vol {
    type: number
    sql: ${TABLE}.vol ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name,
      sgm_periods.name,
      sgm_periods.id
    ]
  }
}
