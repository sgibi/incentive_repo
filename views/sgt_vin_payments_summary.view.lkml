view: sgt_vin_payments_summary {
  sql_table_name: dbo.sgt_vin_payments_summary ;;

  dimension: approved_at {
    type: string
    sql: ${TABLE}.approvedAt ;;
  }

  dimension: approved_by {
    type: string
    sql: ${TABLE}.approvedBy ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dealer_number {
    type: string
    sql: ${TABLE}.DEALER_NUMBER ;;
  }

  dimension: is_approved {
    type: string
    sql: ${TABLE}.IS_APPROVED ;;
  }

  dimension: is_dealership_payee {
    type: string
    sql: ${TABLE}.IS_DEALERSHIP_PAYEE ;;
  }

  dimension: is_processed {
    type: string
    sql: ${TABLE}.IS_PROCESSED ;;
  }

  dimension: payee_id {
    type: string
    sql: ${TABLE}.PAYEE_ID ;;
  }

  dimension: payer_reference_number {
    type: string
    sql: ${TABLE}.PAYER_REFERENCE_NUMBER ;;
  }

  dimension: payment_date {
    type: string
    sql: ${TABLE}.PAYMENT_DATE ;;
  }

  dimension: payout_amount {
    type: number
    sql: ${TABLE}.PAYOUT_AMOUNT ;;
  }

  dimension: platform_message {
    type: string
    sql: ${TABLE}.PLATFORM_MESSAGE ;;
  }

  dimension: platform_payment_status {
    type: number
    sql: ${TABLE}.PLATFORM_PAYMENT_STATUS ;;
  }

  dimension: platform_reference_id {
    type: string
    sql: ${TABLE}.PLATFORM_REFERENCE_ID ;;
  }

  dimension: processed_at {
    type: string
    sql: ${TABLE}.processedAt ;;
  }

  dimension: processed_by {
    type: string
    sql: ${TABLE}.processedBy ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.SGM_CLIENT_ID ;;
  }

  dimension: sgm_dealer_id {
    type: number
    sql: ${TABLE}.SGM_DEALER_ID ;;
  }

  dimension: sgm_period_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.SGM_PERIOD_ID ;;
  }

  dimension: sgm_user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.SGM_USER_ID ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: vin_payments_summary_id {
    type: number
    sql: ${TABLE}.VIN_PAYMENTS_SUMMARY_ID ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      sgm_users.id,
      sgm_users.name,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name,
      sgm_periods.name,
      sgm_periods.id
    ]
  }
}
