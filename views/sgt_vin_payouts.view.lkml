view: sgt_vin_payouts {
  sql_table_name: dbo.sgt_vin_payouts ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.VIn_payouts_ID ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }


  dimension: is_approved {
    type: string
    sql: ${TABLE}.isApproved ;;
  }

  dimension: approved_at {
    type: string
    sql: ${TABLE}.approvedat ;;
  }

  dimension: approved_by {
    type: string
    sql: ${TABLE}.approvedby ;;
  }

  dimension: processedat {
    type: string
    sql: ${TABLE}.processedat ;;
  }

  dimension: processedby {
    type: string
    sql: ${TABLE}.processedby ;;
  }


  dimension: is_processed {
    type: number
    sql: ${TABLE}.isProcessed ;;
  }

  dimension: payout_amount {
    type: number
    sql: ${TABLE}.payout_amount ;;
  }

  measure: total_payout_amount {
    type: number
    sql: sum(${TABLE}.payout_amount) ;;
    drill_fields: [detail*]
  }

  dimension: program_Name {
    type: string
    sql: ${TABLE}.program_name ;;
  }

  dimension: coverage_code {
    hidden: yes
    type: string
    sql: ${TABLE}.coverage_code ;;
  }

  dimension: sgm_dealer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.sgm_dealer_id ;;
  }

  dimension: product_code{
    hidden: yes
    type: string
    sql: ${TABLE}.product_code ;;
  }

  dimension: VIN {
    hidden: yes
    type: string
    sql: ${TABLE}.VIN ;;
  }

  dimension: sgm_client_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgm_Client_Id ;;
  }

  dimension: sgm_period_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgm_Period_Id ;;
  }

  dimension: sgm_program_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgm_Program_Id ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: payout_name {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }


  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      sgt_user_vinsales.sg_con_dealer,
      sgt_user_vinsales.sg_con_lname,
      sgt_user_vinsales.sg_con_contract,
      sgt_user_vinsales.sg_con_vin,
      sgt_user_vinsales.sg_con_plc,
      sgt_user_vinsales.sg_con_saledate_date,
      sgt_user_vinsales.sg_con4_busdate_date,
      sgt_user_vinsales.sg_con4_rectype,
      sgt_user_vinsales.sg_con_term,
      total_payout_amount
    ]
  }
}
