- dashboard: admin_dashboard
  title: Admin Dashboard
  layout: newspaper
  elements:
  - name: Incentive Payouts - Carrier, Region, Dealer, Product, Net Sales, total Payout
    title: Incentive Payouts - Carrier, Region, Dealer, Product, Net Sales, total
      Payout
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: looker_grid
    fields: [sgt_user_vinsales.sg_con4_carrier, dealer_region_market.region, dealer_region_market.market,
      dealer_region_market.dealer_code, sgt_user_vinsales.sg_con_plc, sgt_user_vinsales.total_written_contracts,
      sgt_user_vinsales.total_flat_cancels, sgt_user_vinsales.total_sales_net_flat_cancel,
      sgt_vin_payouts.total_payout_amount]
    filters:
      dealer_region_market.owner_code: PORSCHE USA
      sgm_programs.program_title: ''
    sorts: [sgt_user_vinsales.sg_con4_carrier, dealer_region_market.region, dealer_region_market.market,
      dealer_region_market.dealer_code, sgt_user_vinsales.sg_con_plc]
    subtotals: [dealer_region_market.region, dealer_region_market.market, dealer_region_market.dealer_code,
      sgt_user_vinsales.sg_con4_carrier]
    limit: 5000
    column_limit: 50
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    listen:
      Client: sgm_clients.client_name
      Period: sgm_periods.name
      Dealer: dealer_region_market.dealer_code
      Sales Date: sgt_user_vinsales.sg_con_saledate_date
      Carrier Code: sgt_user_vinsales.sg_con4_carrier
      Product Code: sgt_user_vinsales.sg_con_plc
    row: 11
    col: 0
    width: 24
    height: 7
  - title: Product Mix By Region
    name: Product Mix By Region
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: looker_donut_multiples
    fields: [dealer_region_market.market, sgt_user_vinsales.sg_con_plc, sgt_user_vinsales.total_sales_net_flat_cancel]
    pivots: [sgt_user_vinsales.sg_con_plc]
    filters:
      dealer_region_market.owner_code: PORSCHE USA
      sgm_programs.program_title: PFS-F&I Rewards Program
    sorts: [dealer_region_market.market, sgt_user_vinsales.sg_con_plc]
    limit: 500
    column_limit: 50
    show_value_labels: true
    font_size: 12
    hide_legend: false
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      palette_id: 5378478f-6725-4b04-89cc-75fc42da804e
      options:
        steps: 5
        reverse: false
    series_colors: {}
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    series_types: {}
    listen:
      Client: sgm_clients.client_name
      Period: sgm_periods.name
      Dealer: dealer_region_market.dealer_code
      Sales Date: sgt_user_vinsales.sg_con_saledate_date
      Carrier Code: sgt_user_vinsales.sg_con4_carrier
      Product Code: sgt_user_vinsales.sg_con_plc
    row: 0
    col: 10
    width: 14
    height: 11
  - title: Payouts By State
    name: Payouts By State
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: looker_map
    fields: [sgt_vin_payouts.total_payout_amount, dealer_master.sg_dlr_state]
    filters:
      dealer_region_market.owner_code: PORSCHE USA
      sgm_programs.program_title: PFS-F&I Rewards Program
    sorts: [sgt_vin_payouts.total_payout_amount desc]
    limit: 500
    column_limit: 50
    map_plot_mode: points
    heatmap_gridlines: false
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.5
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: light
    map_position: fit_data
    map_scale_indicator: 'off'
    map_pannable: true
    map_zoomable: true
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_view_names: false
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: true
    map_latitude: 34.86430017292065
    map_longitude: -82.64355361461641
    map_zoom: 4
    show_value_labels: true
    font_size: 12
    hide_legend: false
    color_application:
      collection_id: b43731d5-dc87-4a8e-b807-635bef3948e7
      palette_id: 471a8295-662d-46fc-bd2d-2d0acd370c1e
      options:
        steps: 5
        reverse: true
    series_colors: {}
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    series_types: {}
    listen:
      Client: sgm_clients.client_name
      Period: sgm_periods.name
      Dealer: dealer_region_market.dealer_code
      Sales Date: sgt_user_vinsales.sg_con_saledate_date
      Carrier Code: sgt_user_vinsales.sg_con4_carrier
      Product Code: sgt_user_vinsales.sg_con_plc
    row: 0
    col: 0
    width: 10
    height: 11
  filters:
  - name: Client
    title: Client
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: Incentive_Sales
    explore: sgt_user_vinsales
    listens_to_filters: []
    field: sgm_clients.client_name
  - name: Period
    title: Period
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: Incentive_Sales
    explore: sgt_user_vinsales
    listens_to_filters: []
    field: sgm_periods.name
  - name: Dealer
    title: Dealer
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: Incentive_Sales
    explore: sgt_user_vinsales
    listens_to_filters: []
    field: dealer_region_market.dealer_code
  - name: Sales Date
    title: Sales Date
    type: date_filter
    default_value: ''
    allow_multiple_values: true
    required: false
  - name: Business Date
    title: Business Date
    type: date_filter
    default_value: ''
    allow_multiple_values: true
    required: false
  - name: Carrier Code
    title: Carrier Code
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: Incentive_Sales
    explore: sgt_user_vinsales
    listens_to_filters: []
    field: sgt_user_vinsales.sg_con4_carrier
  - name: Role
    title: Role
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: Incentive_Sales
    explore: sgt_user_vinsales
    listens_to_filters: []
    field: sgm_roles.name
  - name: Product Code
    title: Product Code
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: Incentive_Sales
    explore: sgt_user_vinsales
    listens_to_filters: []
    field: sgt_user_vinsales.sg_con_plc
