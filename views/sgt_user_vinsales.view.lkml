view: sgt_user_vinsales {
  sql_table_name: dbo.sgt_user_vinsales ;;
  drill_fields: [id]
  view_label: "Vin Sales"

  dimension: id {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: checksumval {
    type: string
    hidden: yes
    sql: ${TABLE}.checksumval ;;
  }

  dimension_group: create {
    group_label: "Created"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.Create_Date ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: is_claim {
    type: string
    sql: ${TABLE}.isClaim ;;
  }

  dimension: is_processed {
    type: string
    sql: ${TABLE}.isProcessed ;;
  }

  dimension_group: modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.Modified_Date ;;
  }

  dimension: new_used {
    type: string
    sql: ${TABLE}.New_used ;;
  }

  dimension: original_coverage_code {
    type: string
    sql: ${TABLE}.Original_Coverage_code ;;
  }

  dimension: sg_ac_desc {
    label: "Model"
    type: string
    sql: ${TABLE}.sg_ac_desc ;;
  }

  dimension: sg_can_calcmeth {
    label: "Cancel Calculation Method"
    type: string
    sql: ${TABLE}.sg_can_calcmeth ;;
  }

  dimension: sg_can_cusref {
    label: "Customer Refund"
    type: number
    sql: ${TABLE}.sg_can_cusref ;;
  }

  dimension_group: sg_can {
    group_label: "Cancellation date"
    label: "Cancellation"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.sg_can_date ;;
  }

  dimension: sg_can_dlrref {
    label: "Dealer Refund"
    type: number
    sql: ${TABLE}.sg_can_dlrref ;;
  }

  dimension: sg_can_per {
    label: "Refund Percentage"
    type: number
    sql: ${TABLE}.sg_can_per ;;
  }

  dimension: sg_can_reason {
    label: "Cancellation Reason"
    type: string
    sql: ${TABLE}.sg_can_reason ;;
  }

  dimension_group: sg_con4_busdate {
    group_label: "Business Date"
    label: "Business"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.sg_con4_busdate ;;
  }

  dimension: sg_con4_carrier {
    label: "Carrier Code"
    type: string
    sql: ${TABLE}.sg_con4_carrier ;;
  }

  dimension_group: sg_con4_lockdate {
    group_label: "Locked Date"
    label: "Locked"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.sg_con4_lockdate ;;
  }

  dimension: sg_con4_locked {
    label: "Locked"
    type: string
    sql: ${TABLE}.sg_con4_locked ;;
  }

  dimension_group: sg_con4_postdate {
    label: "Posted Date"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.sg_con4_postdate ;;
  }

  dimension: sg_con4_posted {
    label: "Posted"
    type: string
    sql: ${TABLE}.sg_con4_posted ;;
  }

  dimension: sg_con4_rectype {
    label: "Record Type"
    type: string
    sql: ${TABLE}.sg_con4_rectype ;;
  }

  measure: total_written_contracts {
    type: sum
    description: "Contracts Written only counts sales, and will count multi-coverage Contracs as 1 Record per Coverage"
    sql: case when ${sg_con4_rectype} = 'Written' then 1 else 0 end  ;;
    drill_fields: [detail*]
  }

  measure: total_cancellations {
    type: sum
    description: "Total Cancellations Counts all Cancellations, regardless of Inside or outside of Free look Period"
    sql: case when ${sg_con4_rectype} = 'Cancelled' then 1 else 0 end;;
    drill_fields: [detail*]
  }

  measure: total_flat_cancels {
    type: sum
    description: "Total Cancellations where 100% is refunded to customer and dealer"
    sql: case when ${sg_con4_rectype} = 'Cancelled' and ${sg_can_per} = 1 then 1 else 0 end;;
    drill_fields: [detail*]
  }

  measure: total_sales_net_flat_cancel{
    type: number
    description: "Total Written Contracts minus Flat Cancelled where Cancellation percentage = 1"
    sql: sum(case when ${sg_con4_rectype} = 'Written' then 1 when ${sg_con4_rectype} = 'Cancelled' and ${sg_can_per} = 1 then -1 else 0 end) ;;
    drill_fields: [detail*]
  }


  dimension: sg_con4_seq {
    label: "Contract Sequence"
    type: string
    sql: ${TABLE}.sg_con4_seq ;;
  }

  dimension: sg_con_contract {
    label: "Contract Number"
    type: string
    sql: ${TABLE}.sg_con_contract ;;
  }

  dimension: sg_con_cover {
    label: "Coverage Code"
    type: string
    sql: ${TABLE}.sg_con_cover ;;
  }

  dimension: sg_con_cuscost {
    label: "Customer Cost"
    type: number
    sql: ${TABLE}.sg_con_cuscost ;;
  }

  measure: total_customer_cost {
    type: sum
    sql: ${sg_con_cuscost} ;;
  }

  dimension: sg_con_dealer {
#    hidden: yes
    label: "Dealer Code"
    type: string
    sql: ${TABLE}.sg_con_dealer ;;
  }

  dimension: sg_con_dlrcost {
    label: "Dealer Cost"
    type: number
    sql: ${TABLE}.sg_con_dlrcost ;;
  }

  measure: total_dealer_cost {
    type: sum
    sql: ${sg_con_dlrcost} ;;
  }

  dimension_group: sg_con_expdate {
    group_label: "Contract Expire Date"
    label: "Expire"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.sg_con_expdate ;;
  }

  dimension: sg_con_finamt {
    label: "Finance Amount"
    type: number
    sql: ${TABLE}.sg_con_finamt ;;
  }

  measure: total_finance_amount {
    type: sum
    sql: ${sg_con_finamt} ;;
  }

  dimension: sg_con_form {
    label: "Form Code"
    type: string
    sql: ${TABLE}.sg_con_form ;;
  }

  dimension: sg_con_lf {
    label: "Finance Type"
    type: string
    sql: ${TABLE}.sg_con_lf ;;
  }

  dimension: sg_con_lname {
    label: "Customer last Name"
    type: string
    sql: ${TABLE}.sg_con_lname ;;
  }

  dimension: sg_con_nada {
    type: number
    sql: ${TABLE}.sg_con_nada ;;
  }

  dimension: sg_con_ncic {
    label: "Vehicle Make"
    type: string
    sql: ${TABLE}.sg_con_ncic ;;
  }

  dimension_group: sg_con_origbdate {
    group_label: "Original Business Date"
    label: "Original Business"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.sg_con_origbdate ;;
  }

  dimension: sg_con_plc {
    label: "Product Code"
    type: string
    sql: ${TABLE}.sg_con_plc  ;;
  }

  dimension: Product_Name {
    label: "Product name"
    type: string
    sql: case when ${TABLE}.sg_con_plc = 'SAVS' then 'Safe-Gaurd VSC'
              when ${TABLE}.sg_con_plc = 'SAMS' then 'Safe-Gaurd Multi-Coverage Bundle'
              else ${TABLE}.sg_con_plc end ;;
  }

  dimension: sg_con_rstype {
    label: "Risk Type"
    type: string
    sql: ${TABLE}.sg_con_rstype ;;
  }

  dimension_group: sg_con_saledate {
    group_label: "Sale Date"
    label: "Sale"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.sg_con_saledate ;;
  }

  dimension: sg_con_split {
    label: "Is Split Contract"
    type: string
    sql: ${TABLE}.sg_con_split ;;
  }

  dimension: sg_con_state {
    label: "State"
    type: string
    sql: ${TABLE}.sg_con_state ;;
  }

  dimension: sg_con_status {
    label: "Contract Status"
    type: string
    sql: ${TABLE}.sg_con_status ;;
  }

  dimension: sg_con_term {
    label: "Contract Term"
    type: number
    sql: ${TABLE}.sg_con_term ;;
  }

  dimension: sg_con_type {
    label: "Type"
    hidden: yes
    type: string
    sql: ${TABLE}.sg_con_type ;;
  }

  dimension: sg_con_vehcost {
    label: "Vehicle Cost"
    type: number
    sql: ${TABLE}.sg_con_vehcost ;;
  }

  dimension: sg_con_vin {
    label: "Vehicle Vin"
    type: string
    sql: ${TABLE}.sg_con_vin ;;
  }

  dimension: sg_con_year {
    label: "Model Year"
    type: string
    sql: ${TABLE}.sg_con_year ;;
  }

  dimension: sg_dlr_agent {
    hidden: yes
    label: "Agent Code"
    type: string
    sql: ${TABLE}.sg_dlr_agent ;;
  }

  dimension: sg_splitcon_old {
    label: "Split Contract Number"
    description: "Contract number of original split contract. This contract number will be the same for all contracts sold to the same customer on the same form"
    type: string
    sql: ${TABLE}.sg_splitcon_old ;;
  }

  dimension: sgm_client_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: sgm_user_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgmUserId ;;
  }

  dimension: sgt_vin_payout_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgtVinPayoutId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      sgt_user_vinsales.sg_con_dealer,
      sgt_user_vinsales.sg_con_lname,
      sgt_user_vinsales.sg_con_contract,
      sgt_user_vinsales.sg_con_vin,
      sgt_user_vinsales.sg_con_plc,
      sgt_user_vinsales.sg_con_saledate_date,
      sgt_user_vinsales.sg_con4_busdate_date,
      sgt_user_vinsales.sg_con4_rectype,
      sgt_user_vinsales.sg_con_term,
      sgt_vin_payouts.total_payout_amount,
      sgt_user_vinsales.total_user_payout_amount,
      sgt_dealership_payouts.total_dealerships_payout_amount
    ]
  }
}
