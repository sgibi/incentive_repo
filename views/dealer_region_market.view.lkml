view: dealer_region_market {
  sql_table_name: dbo.sgm_dealer_region_market ;;

  dimension: agent_code {
    label: "Agent Code"
    type: string
    sql: ${TABLE}."Agent Code" ;;
  }

  dimension: current_flag {
    hidden: yes
    type: string
    sql: ${TABLE}."Current Flag" ;;
  }

  dimension: dealer_code {
    label: "Dealer Code"
    type: string
    sql: ${TABLE}."Dealer Code" ;;
  }

  dimension: dealer_company {
    type: string
    sql: ${TABLE}."Dealer Company" ;;
  }

  dimension_group: dealer_geography_end {
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."Dealer Geography End Date" ;;
  }

  dimension: dealer_geography_identifier {
    hidden: yes
    type: number
    sql: ${TABLE}."Dealer Geography Identifier" ;;
  }

  dimension_group: dealer_geography_start {
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."Dealer Geography Start Date" ;;
  }

  dimension: dealer_identifier {
    hidden: yes
    type: number
    sql: ${TABLE}."Dealer Identifier" ;;
  }

  dimension: dealer_manager_name {
    type: string
    sql: ${TABLE}."Dealer Manager Name" ;;
  }

  dimension: load_by {
    hidden: yes
    type: string
    sql: ${TABLE}."Load By" ;;
  }

  dimension_group: load {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."Load Date" ;;
  }

  dimension: market {
    type: string
    sql: ${TABLE}.Market ;;
  }

  dimension: owner_code {
    label: "Brand Owner"
    type: string
    sql: ${TABLE}."Owner Code" ;;
  }

  dimension: owner_identifier {
    hidden: yes
    type: number
    sql: ${TABLE}."Owner Identifier" ;;
  }

  dimension: region {
    type: string
    sql: ${TABLE}.Region ;;
  }

  dimension_group: update {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."Update Date" ;;
  }

  dimension: updated_by {
    hidden: yes
    type: string
    sql: ${TABLE}."Updated By" ;;
  }

  measure: count {
    type: count
    drill_fields: [dealer_code,owner_code,market,region]
  }
}
