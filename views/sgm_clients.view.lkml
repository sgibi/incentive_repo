view: sgm_clients {
  sql_table_name: dbo.sgm_clients ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: client_name {
    label: "Brand\Partner Name"
    type: string
    sql: ${TABLE}.client_name ;;
  }

  dimension: created_at {
    hidden: yes
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    hidden: yes
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dashboard_img {
    label: "Dashboard Image"
    type: string
    sql: ${TABLE}.dashboard_img ;;
  }

  dimension: font_h1_name {
    hidden: yes
    type: string
    sql: ${TABLE}.font_h1_name ;;
  }

  dimension: font_h1_size {
    hidden: yes
    type: string
    sql: ${TABLE}.font_h1_size ;;
  }

  dimension: font_h2_name {
    hidden: yes
    type: string
    sql: ${TABLE}.font_h2_name ;;
  }

  dimension: font_h2_size {
    hidden: yes
    type: string
    sql: ${TABLE}.font_h2_size ;;
  }

  dimension: font_h3_name {
    hidden: yes
    type: string
    sql: ${TABLE}.font_h3_name ;;
  }

  dimension: font_h3_size {
    hidden: yes
    type: string
    sql: ${TABLE}.font_h3_size ;;
  }

  dimension: is_approved {
    type: string
    sql: ${TABLE}.isApproved ;;
  }

  dimension: login_img {
    label: "Login Image"
    type: string
    sql: ${TABLE}.login_img ;;
    html: <img src="https://qtsuatgrpor1:8080/{{value}}" width="auto" height="auto">;;
  }

  dimension: logo_img {
    label: "Logo Image"
    type: string
    sql: ${TABLE}.logo_img ;;
    html: <img src="https://qtsuatgrpor1:8080/{{value}}" width="auto" height="auto">;;
  }

  dimension: pcolor {
    label: "Primary Color"
    type: string
    sql: ${TABLE}.pcolor ;;
    #html: <b style= "Background Color: {{value}}"</b> ;;
  }

  dimension: scolor {
    label: "Secondary Color"
    type: string
    sql: ${TABLE}.scolor ;;
  }

  dimension: updated_at {
    hidden: yes
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    hidden: yes
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [

      sgm_client_contacts.count,
      sgm_client_dimension_headers.count,
      sgm_client_dimensions.count,
      sgm_dealerships.count,
      sgm_dimension_calendar_months.count,
      sgm_groups.count,
      sgm_localizations.count,
      sgm_notifications.count,
      sgm_payers.count,
      sgm_periods.count,
      sgm_products.count,
      sgm_program_allocations.count,
      sgm_programs.count,
      sgm_roles.count,
      sgm_users.count,
      sgm_vehicle_models.count,
      sgt_dealership_payments.count,
      sgt_dealership_payouts.count,
      sgt_dealership_vol.count,
      sgt_notifications.count,
      sgt_program_users.count,
      sgt_user_payments.count,
      sgt_user_payouts.count,
      sgt_user_vinsales.count,
      sgt_vehicle_sales.count,
      sgt_vin_payouts.count,
      sgt_vin_vol.count
    ]
  }
}
