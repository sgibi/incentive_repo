view: sgt_vin_payments_detail {
  sql_table_name: dbo.sgt_vin_payments_detail ;;

  dimension: approved_at {
    type: string
    sql: ${TABLE}.approvedAt ;;
  }

  dimension: approved_by {
    type: string
    sql: ${TABLE}.approvedBy ;;
  }

  dimension: coverage_code {
    hidden: yes
    type: string
    sql: ${TABLE}.COVERAGE_CODE ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dealer_number {
    hidden: yes
    type: string
    sql: ${TABLE}.DEALER_NUMBER ;;
  }

  dimension: is_approved {
    type: yesno
    sql: ${TABLE}.IS_APPROVED = 1;;
  }

  dimension: is_dealership_payee {
    type: yesno
    sql: ${TABLE}.IS_DEALERSHIP_PAYEE = 1;;
  }

  dimension: dealership_payee {
    hidden: yes
    type: string
    sql: ${TABLE}.IS_DEALERSHIP_PAYEE;;
  }

  dimension: is_processed {
    type: yesno
    sql: ${TABLE}.IS_PROCESSED = 1;;
  }

  dimension: payout_amount {
    hidden: yes
    type: number
    sql: ${TABLE}.PAYOUT_AMOUNT ;;
  }

  dimension: payout_name {
    type: string
    sql: ${TABLE}.PAYOUT_NAME ;;
  }

  dimension: processed_at {
    type: string
    sql: ${TABLE}.processedAt ;;
  }

  dimension: processed_by {
    type: string
    sql: ${TABLE}.processedBy ;;
  }

  dimension: product_code {
    hidden: yes
    type: string
    sql: ${TABLE}.PRODUCT_CODE ;;
  }

  dimension: sgm_client_id {
    hidden: yes
    type: number
    sql: ${TABLE}.SGM_CLIENT_ID ;;
  }

  dimension: sgm_dealer_id {
    hidden: yes
    type: number
    sql: ${TABLE}.SGM_DEALER_ID ;;
  }

  dimension: sgm_period_id {
    type: number
    hidden: yes
    sql: ${TABLE}.SGM_PERIOD_ID ;;
  }

  dimension: sgm_program_id {
    type: number
    hidden: yes
    sql: ${TABLE}.SGM_PROGRAM_ID ;;
  }

  dimension: sgm_program_name {
    type: string
    sql: ${TABLE}.SGM_PROGRAM_NAME ;;
  }

  dimension: sgm_user_id {
    type: number
    hidden: yes
    sql: ${TABLE}.SGM_USER_ID ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: vin {
    hidden: yes
    type: string
    sql: ${TABLE}.VIN ;;
  }

  dimension: vin_payments_detail_id {
    hidden: yes
    primary_key: yes
    type: number
    sql: ${TABLE}.VIN_PAYMENTS_DETAIL_ID ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: total_payment_amount{
    type: sum
    drill_fields: [sgt_vin_sales.detail]
    sql: ${payout_amount} ;;
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      sgm_program_name,
      payout_name,


    ]
  }
}
