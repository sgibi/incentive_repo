view: sgt_payout_products {
  sql_table_name: dbo.sgt_payout_products ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: is_count_in_pen_numerator {
    type: string
    sql: ${TABLE}.isCountInPenNumerator ;;
  }

  dimension: payout {
    type: number
    sql: ${TABLE}.payout ;;
  }

  dimension: pen_vol_multiplier {
    type: number
    sql: ${TABLE}.pen_vol_multiplier ;;
  }

  dimension: product {
    type: string
    sql: ${TABLE}.product ;;
  }

  dimension: product_cover {
    type: string
    sql: ${TABLE}.product_cover ;;
  }

  dimension: sgt_program_payout_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgtProgramPayoutId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: vehicle_model {
    type: string
    sql: ${TABLE}.vehicleModel ;;
  }

  dimension: vehicle_type {
    type: string
    sql: ${TABLE}.vehicleType ;;
  }

  measure: count {
    type: count
    drill_fields: [id, sgt_program_payouts.id, sgt_program_payouts.name, sgt_payout_product_tiers.count]
  }
}
