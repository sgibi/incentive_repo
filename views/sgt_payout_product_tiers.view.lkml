view: sgt_payout_product_tiers {
  sql_table_name: dbo.sgt_payout_product_tiers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: sgt_payout_product_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgtPayoutProductId ;;
  }

  dimension: sgt_payout_tier_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgtPayoutTierId ;;
  }

  dimension: tier_payout {
    type: number
    sql: ${TABLE}.tierPayout ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [id, sgt_payout_products.id, sgt_payout_tiers.id]
  }
}
