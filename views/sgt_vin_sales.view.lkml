view: sgt_vin_sales {
  sql_table_name: dbo.sgt_vin_sales ;;

  dimension: agent_number {
    type: string
    sql: ${TABLE}.AGENT_NUMBER ;;
  }

  dimension_group: contract_detail_business {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.CONTRACT_DETAIL_BUSINESS_DATE ;;
  }

  dimension: contract_number {
    type: string
    sql: ${TABLE}.CONTRACT_NUMBER ;;
  }

  dimension_group: contract_sale {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.CONTRACT_SALE_DATE ;;
  }

  dimension: contract_split {
    label: "Is Split Contract"
    type: yesno
    sql: ${TABLE}.CONTRACT_SPLIT = 'y' ;;
  }

  dimension: contract_split_old {
    label: "Split Contract Number"
    type: string
    sql: ${TABLE}.CONTRACT_SPLIT_OLD ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: customer_last_name {
    type: string
    sql: ${TABLE}.CUSTOMER_LAST_NAME ;;
  }

  dimension: dealer_number {
    hidden: yes
    type: string
    sql: ${TABLE}.DEALER_NUMBER ;;
  }


  dimension: dealer_name {
    hidden: yes
    type: string
    sql: ${TABLE}.DEALER_NAME ;;
  }

  dimension: is_processed {
    type: string
    sql: ${TABLE}.IS_PROCESSED ;;
  }

  dimension: original_coverage_code {
    type: string
    sql: ${TABLE}.ORIGINAL_COVERAGE_CODE ;;
  }

  dimension: product_code {

    type: string
    sql: ${TABLE}.PRODUCT_CODE ;;
  }

  dimension: product_coverage {
    hidden: yes
    type: string
    sql: ${TABLE}.PRODUCT_COVERAGE ;;
  }

  dimension: product_coverage_description {
    hidden: yes
    type: string
    sql: ${TABLE}.PRODUCT_COVERAGE_DESCRIPTION ;;
  }

  dimension: record_type {
    type: string
    sql: ${TABLE}.RECORD_TYPE ;;
  }

  dimension: rstype {
    type: string
    sql: ${TABLE}.RSTYPE ;;
  }

  dimension: sgm_client_id {
    type: number
    hidden: yes
    sql: ${TABLE}.SGM_CLIENT_ID ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: vehicle_model {
    type: string
    sql: ${TABLE}.VEHICLE_MODEL ;;
  }

  dimension: vehicle_odometer {
    type: string
    sql: ${TABLE}.VEHICLE_ODOMETER ;;
  }

  dimension: vin {
    type: string
    sql: ${TABLE}.VIN ;;
  }

  dimension: vin_sales_id {
    hidden: yes
    primary_key: yes
    type: number
    sql: ${TABLE}.VIN_SALES_ID ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: Net_Sales {
    type: sum
    description: "Sales minus Cancellations"
    sql: case when ${record_type} in ('Written', 'Re-Instated', 'Positive Adjustment') then 1
      else -1 end  ;;
  }


  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      sgm_dealerships.sg_dlr_dealer,
      sgm_dealerships.sg_dlr_company,
      sgm_users.name,
      contract_number,
      contract_sale_date,
      contract_detail_business_date,
      record_type,
      sgm_product_coverage.product_description,
      sgm_Product_coverage.coverage_description,
      vin,
      vehicle_model,
      sgt_vin_payments_detail.total_payment_amount
    ]
  }
}
