view: sgt_vin_vol {
  sql_table_name: dbo.sgt_vin_vol ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: sg_con4_cover {
    type: string
    sql: ${TABLE}.sg_con4_cover ;;
  }

  dimension: sg_con_dealer {
    type: string
    sql: ${TABLE}.sg_con_dealer ;;
  }

  dimension: sg_con_plc {
    type: string
    sql: ${TABLE}.sg_con_plc ;;
  }

  dimension_group: sg_con_saledate {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.sg_con_saledate ;;
  }

  dimension: sg_con_vin {
    type: string
    sql: ${TABLE}.sg_con_vin ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: sgm_period_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmPeriodId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: vin_vol {
    type: number
    sql: ${TABLE}.vin_vol ;;
  }

  dimension: vol_pen_multiplier {
    type: number
    sql: ${TABLE}.vol_pen_multiplier ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name,
      sgm_periods.name,
      sgm_periods.id
    ]
  }
}
