view: sgt_vehicle_sales {
  sql_table_name: dbo.sgt_vehicle_sales ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: agent_code {
    type: string
    sql: ${TABLE}.Agent_Code ;;
  }

  dimension: checksumval {
    type: string
    sql: ${TABLE}.checksumval ;;
  }

  dimension: current_flag {
    type: string
    sql: ${TABLE}.current_flag ;;
  }

  dimension: dealer_code {
    type: string
    sql: ${TABLE}.Dealer_Code ;;
  }

  dimension: delete_flag {
    type: string
    sql: ${TABLE}.Delete_Flag ;;
  }

  dimension: dw_crte_usrid {
    type: string
    sql: ${TABLE}.dw_crte_usrid ;;
  }

  dimension_group: dw_eff_end_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.Dw_eff_end_dttm ;;
  }

  dimension_group: dw_eff_strt_dttm {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.dw_eff_strt_dttm ;;
  }

  dimension: finance_apr {
    type: number
    sql: ${TABLE}.Finance_APR ;;
  }

  dimension: finance_term {
    type: number
    sql: ${TABLE}.Finance_Term ;;
  }

  dimension_group: inservice {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.Inservice_Date ;;
  }

  dimension: manufacturer {
    type: string
    sql: ${TABLE}.Manufacturer ;;
  }

  dimension: msrp {
    type: number
    sql: ${TABLE}.MSRP ;;
  }

  dimension: new_used {
    type: string
    sql: ${TABLE}.New_Used ;;
  }

  dimension: odometer {
    type: number
    sql: ${TABLE}.Odometer ;;
  }

  dimension: rectype {
    type: string
    sql: ${TABLE}.Rectype ;;
  }

  dimension: sale_count {
    type: number
    sql: ${TABLE}.Sale_Count ;;
  }

  dimension_group: sale {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.Sale_Date ;;
  }

  dimension: sale_type {
    type: string
    sql: ${TABLE}.Sale_Type ;;
  }

  dimension: sequence {
    type: string
    sql: ${TABLE}.Sequence ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: vehicle_finance_type {
    type: string
    sql: ${TABLE}.Vehicle_Finance_Type ;;
  }

  dimension: vehicle_model {
    type: string
    sql: ${TABLE}.Vehicle_Model ;;
  }

  dimension: vehicle_sale_price {
    type: number
    sql: ${TABLE}.Vehicle_Sale_Price ;;
  }

  dimension: vin {
    type: string
    sql: ${TABLE}.VIN ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name
    ]
  }
}
