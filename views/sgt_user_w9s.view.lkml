view: sgt_user_w9s {
  sql_table_name: dbo.sgt_user_w9s ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_name {
    type: string
    sql: ${TABLE}.accountName ;;
  }

  dimension: account_number {
    type: string
    sql: ${TABLE}.accountNumber ;;
  }

  dimension: account_type {
    type: string
    sql: ${TABLE}.accountType ;;
  }

  dimension: address {
    type: string
    sql: ${TABLE}.address ;;
  }

  dimension: apt {
    type: string
    sql: ${TABLE}.apt ;;
  }

  dimension: business_name {
    type: string
    sql: ${TABLE}.businessName ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: direct_deposit {
    type: string
    sql: ${TABLE}.directDeposit ;;
  }

  dimension: e_signature {
    type: string
    sql: ${TABLE}.eSignature ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.firstName ;;
  }

  dimension: full_name {
    type: string
    sql: ${TABLE}.fullName ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.lastName ;;
  }

  dimension: onboard_response {
    type: string
    sql: ${TABLE}.onboardResponse ;;
  }

  dimension: payee_id {
    type: string
    sql: ${TABLE}.payeeId ;;
  }

  dimension: payer_id {
    type: string
    sql: ${TABLE}.payerId ;;
  }

  dimension: rewards_card {
    type: string
    sql: ${TABLE}.rewardsCard ;;
  }

  dimension: routing_number {
    type: string
    sql: ${TABLE}.routingNumber ;;
  }

  dimension: sgm_user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmUserId ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: tax_id {
    type: string
    sql: ${TABLE}.taxId ;;
  }

  dimension: tax_id_type {
    type: string
    sql: ${TABLE}.taxIdType ;;
  }

  dimension: tin_check_response {
    type: string
    sql: ${TABLE}.tinCheckResponse ;;
  }

  dimension: today_date {
    type: string
    sql: ${TABLE}.todayDate ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: zip {
    type: zipcode
    sql: ${TABLE}.zip ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      business_name,
      account_name,
      first_name,
      last_name,
      full_name,
      sgm_users.id,
      sgm_users.name
    ]
  }
}
