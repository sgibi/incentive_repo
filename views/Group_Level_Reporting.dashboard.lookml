- dashboard: group_level_reporting
  title: Group Level Reporting
  layout: newspaper
  elements:
  - name: Incentive Payouts - Carrier, Region, Dealer, Product, Net Sales, total Payout
    title: Incentive Payouts - Carrier, Region, Dealer, Product, Net Sales, total
      Payout
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: looker_grid
    fields: [sgt_user_vinsales.sg_con4_carrier, dealer_region_market.region, dealer_region_market.market,
      dealer_region_market.dealer_code, sgt_user_vinsales.sg_con_plc, sgt_user_vinsales.total_written_contracts,
      sgt_user_vinsales.total_flat_cancels, sgt_user_vinsales.total_sales_net_flat_cancel,
      sgt_vin_payouts.total_payout_amount]
    filters:
      dealer_region_market.owner_code: PORSCHE USA
      dealer_region_market.dealer_code: ''
      sgm_periods.name: ''
      sgm_programs.program_title: ''
    sorts: [sgt_user_vinsales.sg_con4_carrier, dealer_region_market.region, dealer_region_market.market,
      dealer_region_market.dealer_code, sgt_user_vinsales.sg_con_plc]
    subtotals: [dealer_region_market.region, dealer_region_market.market, dealer_region_market.dealer_code,
      sgt_user_vinsales.sg_con4_carrier]
    limit: 5000
    column_limit: 50
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    row: 0
    col: 0
    width: 24
    height: 10
