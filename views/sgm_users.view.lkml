view: sgm_users {
  sql_table_name: dbo.sgm_users ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: address {
    type: string
    sql: ${TABLE}.address ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: is_deleted {
    label: "Is Deactivated"
    type: string
    sql: ${TABLE}.isDeleted ;;
  }

  dimension: name {
    type: string
    sql: case when ${TABLE}.name is null then 'Unclaimed' else ${TABLE}.name end;;
  }

  dimension: password {
    hidden: yes
    type: string
    sql: ${TABLE}.password ;;
  }

  dimension: phone_number {
    type: string
    sql: ${TABLE}.phoneNumber ;;
  }

  dimension: primary_dealership_id {
    hidden: yes
    type: number
    sql: ${TABLE}.primaryDealershipId ;;
  }

  dimension: profile_image_path {
    label: "Profile Image"
    type: string
    sql: case when  ${TABLE}.profileImagePath is null then 'https://incentivesapi-demo.incentivesportals.com/images/10245_profile.png'
              else 'https://incentivesapi-demo.incentivesportals.com/' + ${TABLE}.profileImagePath end ;;
    html: <img src="{{value}}" width="125" height="auto">;;
  }#test


  dimension: registration_number {
    type: string
    sql: ${TABLE}.registrationNumber ;;
  }

  dimension: Reset_Password_Number {
    type: string
    sql: ${TABLE}.resetPasswordNumber ;;
  }

  dimension: sgm_client_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: user_w9_ {
    type: string
    sql: ${TABLE}.userw9id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name,
      sgt_dealership_gms.count,
      sgt_notifications.count,
      sgt_program_users.count,
      sgt_user_payments.count,
      sgt_user_payouts.count,
      sgt_user_vinsales.count,
      sgt_user_w9s.count
    ]
  }
}
