view: sgm_client_dimensions {
  sql_table_name: dbo.sgm_client_dimensions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: agent {
    type: string
    sql: ${TABLE}.Agent ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: current {
    type: string
    sql: ${TABLE}."Current" ;;
  }

  dimension: dealer {
    type: string
    sql: ${TABLE}.Dealer ;;
  }

  dimension: dimension_value {
    type: string
    sql: ${TABLE}."Dimension Value" ;;
  }

  dimension_group: effective {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."Effective Date" ;;
  }

  dimension: end_edate {
    type: string
    sql: ${TABLE}."End Edate" ;;
  }

  dimension: oem_dimension_name {
    type: string
    sql: ${TABLE}."OEM Dimension Name" ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      oem_dimension_name,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name
    ]
  }
}
