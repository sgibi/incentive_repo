view: sgm_roles {
  sql_table_name: dbo.sgm_roles ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: accounting_w9 {
    type: string
    sql: ${TABLE}.accountingW9 ;;
  }

  dimension: allocation {
    type: string
    sql: ${TABLE}.allocation ;;
  }

  dimension: bank_account {
    type: string
    sql: ${TABLE}.bankAccount ;;
  }

  dimension: branding_approval {
    type: string
    sql: ${TABLE}.brandingApproval ;;
  }

  dimension: branding_approval_count {
    type: number
    sql: ${TABLE}.brandingApprovalCount ;;
  }

  dimension: calendar_period {
    type: string
    sql: ${TABLE}.calendarPeriod ;;
  }

  dimension: claim_vinsales {
    type: string
    sql: ${TABLE}.claimVINSales ;;
  }

  dimension: client_branding {
    type: string
    sql: ${TABLE}.clientBranding ;;
  }

  dimension: contacts {
    type: string
    sql: ${TABLE}.contacts ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dealer_payment_approval {
    type: string
    sql: ${TABLE}.dealerPaymentApproval ;;
  }

  dimension: dealer_payment_approval_count {
    type: number
    sql: ${TABLE}.dealerPaymentApprovalCount ;;
  }

  dimension: dealer_reports {
    type: string
    sql: ${TABLE}.dealerReports ;;
  }

  dimension: dealer_user_payment {
    type: string
    sql: ${TABLE}.dealerUserPayment ;;
  }

  dimension: dealer_user_payout {
    type: string
    sql: ${TABLE}.dealerUserPayout ;;
  }

  dimension: enrollment {
    type: string
    sql: ${TABLE}.enrollment ;;
  }

  dimension: fi_reports {
    type: string
    sql: ${TABLE}.fiReports ;;
  }

  dimension: group_reports {
    type: string
    sql: ${TABLE}.groupReports ;;
  }

  dimension: groups {
    type: string
    sql: ${TABLE}.groups ;;
  }

  dimension: incentive_programs {
    type: string
    sql: ${TABLE}.incentivePrograms ;;
  }

  dimension: is_deleted {
    type: string
    sql: ${TABLE}.isDeleted ;;
  }

  dimension: localization {
    type: string
    sql: ${TABLE}.localization ;;
  }

  dimension: manage_alerts {
    type: string
    sql: ${TABLE}.manageAlerts ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: payout_approval {
    type: string
    sql: ${TABLE}.payoutApproval ;;
  }

  dimension: payout_approval_count {
    type: number
    sql: ${TABLE}.payoutApprovalCount ;;
  }

  dimension: program_approval {
    type: string
    sql: ${TABLE}.programApproval ;;
  }

  dimension: program_approval_count {
    type: number
    sql: ${TABLE}.programApprovalCount ;;
  }

  dimension: reasign_vinsales {
    type: string
    sql: ${TABLE}.reasignVINSales ;;
  }

  dimension: roles {
    type: string
    sql: ${TABLE}.roles ;;
  }

  dimension: sgm_client_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: user_payment_approval {
    type: string
    sql: ${TABLE}.userPaymentApproval ;;
  }

  dimension: user_payment_approval_count {
    type: number
    sql: ${TABLE}.userPaymentApprovalCount ;;
  }

  dimension: users {
    type: string
    sql: ${TABLE}.users ;;
  }

  dimension: vin_payout {
    type: string
    sql: ${TABLE}.vinPayout ;;
  }

  dimension: vin_payout_approval {
    type: string
    sql: ${TABLE}.vinPayoutApproval ;;
  }

  dimension: vin_payout_approval_count {
    type: number
    sql: ${TABLE}.vinPayoutApprovalCount ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name
    ]
  }
}
