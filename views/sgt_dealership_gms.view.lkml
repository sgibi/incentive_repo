view: sgt_dealership_gms {
  sql_table_name: dbo.sgt_dealership_gms ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: is_active {
    type: string
    sql: ${TABLE}.isActive ;;
  }

  dimension: sgm_dealership_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmDealershipId ;;
  }

  dimension: sgm_user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmUserId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_users.id,
      sgm_users.name,
      sgm_dealerships.id,
      sgm_dealerships.dealer_manager_name,
      sgm_dealerships.geography_level_1_name,
      sgm_dealerships.geography_level_2_name,
      sgm_dealerships.geography_level_3_name,
      sgm_dealerships.geography_level_4_name
    ]
  }
}
