view: sgt_payout_tiers {
  sql_table_name: dbo.sgt_payout_tiers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: is_retro {
    type: string
    sql: ${TABLE}.isRetro ;;
  }

  dimension: operation {
    type: number
    sql: ${TABLE}.operation ;;
  }

  dimension: sgt_program_payout_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgtProgramPayoutId ;;
  }

  dimension: tier {
    type: number
    sql: ${TABLE}.tier ;;
  }

  dimension: tier_secondary {
    type: number
    sql: ${TABLE}.tierSecondary ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [id, sgt_program_payouts.id, sgt_program_payouts.name, sgt_payout_product_tiers.count]
  }
}
