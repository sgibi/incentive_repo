view: sgt_user_dealerships {
  sql_table_name: dbo.sgt_user_dealerships ;;

  dimension: created_at {
    hidden: yes
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    hidden: yes
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dealership_id {
    type: number
    sql: ${TABLE}.dealershipId ;;
  }

  dimension: updated_at {
    hidden: yes
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    hidden: yes
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: user_id {
    type: number
    sql: ${TABLE}.userId ;;
  }

  dimension: primary_key {
    primary_key: yes
    type: string
    sql: ${user_id} + ${dealership_id} ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}
