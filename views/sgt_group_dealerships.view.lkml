view: sgt_group_dealerships {
  sql_table_name: dbo.sgt_group_dealerships ;;

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dealership_id {
    type: number
    sql: ${TABLE}.dealershipId ;;
  }

  dimension: group_id {
    type: number
    sql: ${TABLE}.groupId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
