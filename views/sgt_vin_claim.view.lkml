view: sgt_vin_claim {
  sql_table_name: dbo.sgt_vin_claim ;;

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dealer_number {
    type: string
    sql: ${TABLE}.DEALER_NUMBER ;;
  }

  dimension: sgm_client_id {
    type: number
    sql: ${TABLE}.SGM_CLIENT_ID ;;
  }

  dimension: sgm_period_id {
    type: number
    sql: ${TABLE}.SGM_PERIOD_ID ;;
  }

  dimension: sgm_user_id {
    type: number
    sql: ${TABLE}.SGM_USER_ID ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: vin {
    type: string
    sql: ${TABLE}.VIN ;;
  }

  dimension: vin_sales_id {
    type: number
    sql: ${TABLE}.VIN_SALES_ID ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
