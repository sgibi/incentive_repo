view: sgt_user_payments {
  sql_table_name: dbo.sgt_user_payments ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: date {
    type: string
    sql: ${TABLE}.date ;;
  }

  dimension: is_approved {
    type: string
    sql: ${TABLE}.isApproved ;;
  }

  dimension: is_processed {
    type: number
    sql: ${TABLE}.isProcessed ;;
  }

  dimension: payer_reference_number {
    type: string
    sql: ${TABLE}.PayerReferenceNumber ;;
  }

  dimension: payment_date {
    type: string
    sql: ${TABLE}.PaymentDate ;;
  }

  dimension: platform_message {
    type: string
    sql: ${TABLE}.PlatformMessage ;;
  }

  dimension: platform_payment_status {
    type: number
    sql: ${TABLE}.PlatformPaymentStatus ;;
  }

  dimension: platform_reference_id {
    type: string
    sql: ${TABLE}.PlatformReferenceId ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: sgm_period_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmPeriodId ;;
  }

  dimension: sgm_user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmUserId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_users.id,
      sgm_users.name,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name,
      sgm_periods.name,
      sgm_periods.id,
      sgt_user_payouts.count
    ]
  }
}
