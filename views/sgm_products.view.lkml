view: sgm_products {
  sql_table_name: dbo.sgm_products ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    hidden: yes
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: sg_cov4_alert {
    label: "Alert Coverage"
    type: string
    sql: ${TABLE}.sg_cov4_alert ;;
  }

  dimension: sg_cov4_cover {
    label: "Coverage Code"
    type: string
    sql: ${TABLE}.sg_cov4_cover ;;
  }

  dimension: sg_cov4_dent {
    label: "Dent Coverage"
    type: string
    sql: ${TABLE}.sg_cov4_dent ;;
  }

  dimension: sg_cov4_etch {
    label: "Theft Coverage"
    type: string
    sql: ${TABLE}.sg_cov4_etch ;;
  }

  dimension: sg_cov4_gap {
    label: "Gap Coverage"
    type: string
    sql: ${TABLE}.sg_cov4_gap ;;
  }

  dimension: sg_cov4_key {
    label: "Key Coverage"
    type: string
    sql: ${TABLE}.sg_cov4_key ;;
  }

  dimension: sg_cov4_lease {
    label: "Excessive Wear and Use Coverage"
    type: string
    sql: ${TABLE}.sg_cov4_lease ;;
  }

  dimension: sg_cov4_maint {
    label: "Maintenance Coverage"
    type: string
    sql: ${TABLE}.sg_cov4_maint ;;
  }

  dimension: sg_cov4_pro {
    label: "Pro coverage"
    type: string
    sql: ${TABLE}.sg_cov4_pro ;;
  }

  dimension: sg_cov4_road {
    label: "Roadside Coverage"
    type: string
    sql: ${TABLE}.sg_cov4_road ;;
  }

  dimension: sg_cov4_tire {
    label: "Tire Coverage"
    type: string
    sql: ${TABLE}.sg_cov4_tire ;;
  }

  dimension: sg_cov4_wind {
    label: "Windshield Coverage"
    type: string
    sql: ${TABLE}.sg_cov4_wind ;;
  }

  dimension: sg_dlr1_plc {
    label: "Product Code"
    type: string
    sql: ${TABLE}.sg_dlr1_plc ;;
  }

  dimension: sg_dlr_agent {
    label: "Agent Code"
    type: string
    sql: ${TABLE}.sg_dlr_agent ;;
  }

  dimension: sg_plc_desc {
    label: "Product Description"
    type: string
    sql: ${TABLE}.sg_plc_desc ;;
  }

  dimension: sg_plc_rstype {
    label: "Risk Type"
    type: string
    sql: ${TABLE}.sg_plc_rstype ;;
  }

  dimension: sgm_client_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension_group: updated {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    hidden: yes
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name
    ]
  }
}
