- dashboard: incentive_participant_dashboard
  title: Incentive Participant Dashboard
  layout: newspaper
  elements:
  - title: Product Mix
    name: Product Mix
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: looker_pie
    fields: [sgt_user_vinsales.sg_con_plc, sgt_user_vinsales.total_sales_net_flat_cancel]
    filters:
      dealer_region_market.owner_code: PORSCHE USA
      dealer_region_market.dealer_code: ''
      sgm_periods.name: ''
      sgm_programs.program_title: PFS-F&I Rewards Program
    sorts: [sgt_user_vinsales.total_sales_net_flat_cancel desc]
    limit: 500
    column_limit: 50
    value_labels: labels
    label_type: labVal
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      custom:
        id: 7cf6ab46-08de-8dae-f7b6-150741a23d7d
        label: Custom
        type: continuous
        stops:
        - color: "#ffcbc5"
          offset: 0
        - color: "#FC2E31"
          offset: 100
      options:
        steps: 5
        reverse: true
    series_colors: {}
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    series_types: {}
    hidden_fields: []
    row: 0
    col: 11
    width: 13
    height: 8
  - title: Net Sales + Payouts By Employee
    name: Net Sales + Payouts By Employee
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: looker_grid
    fields: [sgm_users.profile_image_path, sgm_users.name, sgt_user_vinsales.total_sales_net_flat_cancel,
      sgt_vin_payouts.total_payout_amount, sgm_roles.name]
    filters:
      dealer_region_market.owner_code: PORSCHE USA
      dealer_region_market.dealer_code: ''
      sgm_periods.name: ''
      sgm_programs.program_title: ''
    sorts: [sgt_vin_payouts.total_payout_amount desc]
    limit: 500
    column_limit: 50
    show_view_names: false
    show_row_numbers: false
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: false
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      custom:
        id: 7cf6ab46-08de-8dae-f7b6-150741a23d7d
        label: Custom
        type: continuous
        stops:
        - color: "#ffcbc5"
          offset: 0
        - color: "#FC2E31"
          offset: 100
      options:
        steps: 5
        reverse: true
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    series_labels:
      sgm_roles.name: Role
    series_cell_visualizations:
      sgt_user_vinsales.total_sales_net_flat_cancel:
        is_active: false
    value_labels: labels
    label_type: labVal
    series_colors: {}
    defaults_version: 1
    series_types: {}
    hidden_fields: []
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    row: 0
    col: 0
    width: 11
    height: 8
  - title: Sales Details
    name: Sales Details
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: looker_grid
    fields: [dealer_region_market.dealer_company, sgt_user_vinsales.sg_con_plc, sgt_user_vinsales.total_written_contracts,
      sgt_user_vinsales.total_flat_cancels, sgt_user_vinsales.total_sales_net_flat_cancel,
      sgt_vin_payouts.total_payout_amount, sgm_users.name]
    filters:
      dealer_region_market.owner_code: PORSCHE USA
      dealer_region_market.dealer_code: ''
      sgm_periods.name: ''
      sgm_programs.program_title: ''
    sorts: [dealer_region_market.dealer_company, sgt_user_vinsales.sg_con_plc]
    subtotals: [dealer_region_market.dealer_company, sgt_user_vinsales.sg_con_plc]
    limit: 500
    column_limit: 50
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    listen: {}
    row: 8
    col: 0
    width: 24
    height: 8
