view: sgt_dealership_payments {
  sql_table_name: dbo.sgt_dealership_payments ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: date {
    type: string
    sql: ${TABLE}.date ;;
  }

  dimension: is_approved {
    type: string
    sql: ${TABLE}.isApproved ;;
  }

  dimension: is_processed {
    type: number
    sql: ${TABLE}.isProcessed ;;
  }

  dimension: payer_reference_number {
    type: string
    sql: ${TABLE}.PayerReferenceNumber ;;
  }

  dimension: payment_date {
    type: string
    sql: ${TABLE}.PaymentDate ;;
  }

  dimension: platform_message {
    type: string
    sql: ${TABLE}.PlatformMessage ;;
  }

  dimension: platform_payment_status {
    type: number
    sql: ${TABLE}.PlatformPaymentStatus ;;
  }

  dimension: platform_reference_id {
    type: string
    sql: ${TABLE}.PlatformReferenceId ;;
  }

  dimension: sg_con_dealer {
    type: string
    sql: ${TABLE}.sg_con_dealer ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: sgm_dealership_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmDealershipId ;;
  }

  dimension: sgm_period_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmPeriodId ;;
  }

  dimension: sgm_program_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmProgramId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_dealerships.id,
      sgm_dealerships.dealer_manager_name,
      sgm_dealerships.geography_level_1_name,
      sgm_dealerships.geography_level_2_name,
      sgm_dealerships.geography_level_3_name,
      sgm_dealerships.geography_level_4_name,
      sgm_programs.id,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name,
      sgm_periods.name,
      sgm_periods.id
    ]
  }
}
