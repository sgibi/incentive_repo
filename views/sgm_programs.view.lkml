view: sgm_programs {
  sql_table_name: dbo.sgm_programs ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: end_date {
    type: string
    sql: ${TABLE}.endDate ;;
  }

  dimension: faq_pdf {
    hidden: yes
    type: string
    sql: ${TABLE}.faqPDF ;;
  }

  dimension: is_approved {
    type: string
    sql: ${TABLE}.isApproved ;;
  }

  dimension: is_deleted {
    type: string
    sql: ${TABLE}.isDeleted ;;
  }

  dimension: overview {
    type: string
    sql: ${TABLE}.overview ;;
  }

  dimension: program_title {
    type: string
    sql: ${TABLE}.programTitle ;;
  }

  dimension: rules_pdf {
    hidden: yes
    type: string
    sql: ${TABLE}.rulesPDF ;;
  }

  dimension: sgm_client_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: start_date {
    type: string
    sql: ${TABLE}.startDate ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name,
      sgm_program_allocations.count,
      sgt_dealership_payments.count,
      sgt_dealership_payouts.count,
      sgt_program_payouts.count,
      sgt_program_users.count,
      sgt_user_payouts.count,
      sgt_vin_payouts.count
    ]
  }
}
