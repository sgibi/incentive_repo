- dashboard: dealer_principal_dashboard
  title: Dealer Principal Dashboard
  layout: newspaper
  preferred_viewer: dashboards
  elements:
  - title: Sale and Payouts By Participant
    name: Sale and Payouts By Participant
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: looker_grid
    fields: [sgm_users.profile_image_path, sgm_users.name, sgm_roles.name, sgt_user_vinsales.total_sales_net_flat_cancel,
      sgt_vin_payouts.total_payout_amount]
    filters:
      dealer_region_market.owner_code: PORSCHE USA
      dealer_region_market.dealer_code: ''
      sgm_periods.name: ''
      sgm_programs.program_title: ''
    sorts: [sgt_vin_payouts.total_payout_amount desc]
    limit: 500
    column_limit: 50
    show_view_names: false
    show_row_numbers: false
    transpose: false
    truncate_text: false
    hide_totals: false
    hide_row_totals: false
    size_to_fit: false
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: center
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      palette_id: 18d0c733-1d87-42a9-934f-4ba8ef81d736
      options:
        steps: 5
        reverse: true
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    series_labels:
      sgm_roles.name: Role
    series_cell_visualizations:
      sgt_user_vinsales.total_sales_net_flat_cancel:
        is_active: false
        value_display: true
        palette:
          palette_id: 93ccbaea-4297-9428-2019-9be26a1a8011
          collection_id: b43731d5-dc87-4a8e-b807-635bef3948e7
          custom_colors:
          - "#e60e14"
          - "#FBB555"
          - "#ffffff"
          - "#73d685"
          - "#09bf1e"
      sgt_vin_payouts.total_payout_amount:
        is_active: false
        palette:
          palette_id: 1e4d66b9-f066-4c33-b0b7-cc10b4810688
          collection_id: b43731d5-dc87-4a8e-b807-635bef3948e7
    series_text_format:
      sgt_user_vinsales.total_sales_net_flat_cancel:
        align: center
      sgt_vin_payouts.total_payout_amount:
        align: center
      sgm_users.name:
        align: center
      sgm_users.profile_image_path:
        align: center
      sgm_roles.name:
        align: left
    conditional_formatting: [{type: along a scale..., value: !!null '', background_color: !!null '',
        font_color: !!null '', color_application: {collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7,
          palette_id: a8099e89-1c44-43dd-a3b4-7b76fdc3e338}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    series_value_format:
      sgt_vin_payouts.total_payout_amount:
        name: usd_0
        format_string: "$#,##0"
        label: U.S. Dollars (0)
    value_labels: labels
    label_type: labVal
    series_colors: {}
    defaults_version: 1
    series_types: {}
    hidden_fields: []
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    truncate_column_names: false
    listen:
      User ID: sgt_user_payouts.id
      Dealership ID: sgm_dealerships.id
    row: 0
    col: 0
    width: 11
    height: 19
  - title: Sales Details
    name: Sales Details
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: looker_grid
    fields: [dealer_region_market.dealer_company, sgt_user_vinsales.sg_con_plc, sgt_user_vinsales.total_written_contracts,
      sgt_user_vinsales.total_flat_cancels, sgt_user_vinsales.total_sales_net_flat_cancel,
      sgt_vin_payouts.total_payout_amount, sgm_users.name]
    filters:
      dealer_region_market.owner_code: PORSCHE USA
      dealer_region_market.dealer_code: ''
      sgm_periods.name: ''
      sgm_programs.program_title: ''
    sorts: [dealer_region_market.dealer_company, sgt_user_vinsales.sg_con_plc]
    subtotals: [dealer_region_market.dealer_company, sgt_user_vinsales.sg_con_plc]
    limit: 500
    column_limit: 50
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    listen:
      User ID: sgt_user_payouts.id
      Dealership ID: sgm_dealerships.id
    row: 19
    col: 0
    width: 24
    height: 8
  - title: Total Dealership Earnings (All Payees)
    name: Total Dealership Earnings (All Payees)
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: single_value
    fields: [sgt_vin_payouts.total_payout_amount]
    limit: 500
    query_timezone: America/New_York
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      palette_id: 18d0c733-1d87-42a9-934f-4ba8ef81d736
    custom_color: "#08B248"
    value_format: "$#,##0.00"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: !!null '',
        font_color: !!null '', color_application: {collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7,
          palette_id: a8099e89-1c44-43dd-a3b4-7b76fdc3e338}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    listen:
      User ID: sgt_user_payouts.id
      Dealership ID: sgm_dealerships.id
    row: 5
    col: 11
    width: 7
    height: 6
  - title: Total Payments to Participants
    name: Total Payments to Participants
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: single_value
    fields: [sgt_user_payouts.total_user_payout_amount]
    limit: 500
    query_timezone: America/New_York
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      palette_id: 18d0c733-1d87-42a9-934f-4ba8ef81d736
    custom_color: "#08B248"
    value_format: "$#,##0.00"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: !!null '',
        font_color: !!null '', color_application: {collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7,
          palette_id: a8099e89-1c44-43dd-a3b4-7b76fdc3e338}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    listen:
      User ID: sgt_user_payouts.id
      Dealership ID: sgm_dealerships.id
    row: 0
    col: 18
    width: 6
    height: 5
  - title: Total Payments to Dealership
    name: Total Payments to Dealership
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: single_value
    fields: [sgt_dealership_payouts.total_dealerships_payout_amount]
    limit: 500
    query_timezone: America/New_York
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      palette_id: 18d0c733-1d87-42a9-934f-4ba8ef81d736
    custom_color: "#08B248"
    value_format: "$#,##0.00"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: !!null '',
        font_color: !!null '', color_application: {collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7,
          palette_id: a8099e89-1c44-43dd-a3b4-7b76fdc3e338}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    series_types: {}
    listen:
      User ID: sgt_user_payouts.id
      Dealership ID: sgm_dealerships.id
    row: 0
    col: 11
    width: 7
    height: 5
  - title: Total Incentive payments Made
    name: Total Incentive payments Made
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: single_value
    fields: [sgt_dealership_payouts.total_dealerships_payout_amount, sgt_user_payouts.total_user_payout_amount]
    limit: 500
    dynamic_fields: [{table_calculation: total_payments_made, label: Total Payments
          Made, expression: "${sgt_dealership_payouts.total_dealerships_payout_amount}+${sgt_user_payouts.total_user_payout_amount}",
        value_format: !!null '', value_format_name: !!null '', _kind_hint: measure,
        _type_hint: number}]
    query_timezone: America/New_York
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      palette_id: 18d0c733-1d87-42a9-934f-4ba8ef81d736
    custom_color: "#08B248"
    value_format: "$#,##0.00"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: !!null '',
        font_color: !!null '', color_application: {collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7,
          palette_id: a8099e89-1c44-43dd-a3b4-7b76fdc3e338}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    hidden_fields: [sgt_user_payouts.total_user_payout_amount, sgt_dealership_payouts.total_dealerships_payout_amount]
    listen:
      User ID: sgt_user_payouts.id
      Dealership ID: sgm_dealerships.id
    row: 5
    col: 18
    width: 6
    height: 6
  - title: Product Mix
    name: Product Mix
    model: Incentive_Sales
    explore: sgt_user_vinsales
    type: looker_pie
    fields: [sgt_user_vinsales.sg_con_plc, sgt_user_vinsales.total_sales_net_flat_cancel]
    filters:
      dealer_region_market.owner_code: PORSCHE USA
      dealer_region_market.dealer_code: ''
      sgm_periods.name: ''
      sgm_programs.program_title: ''
    sorts: [sgt_user_vinsales.total_sales_net_flat_cancel desc]
    limit: 500
    column_limit: 50
    value_labels: labels
    label_type: labVal
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      custom:
        id: 7cf6ab46-08de-8dae-f7b6-150741a23d7d
        label: Custom
        type: continuous
        stops:
        - color: "#ffcbc5"
          offset: 0
        - color: "#FC2E31"
          offset: 100
      options:
        steps: 5
        reverse: true
    series_colors: {}
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    series_types: {}
    hidden_fields: []
    listen:
      User ID: sgt_user_payouts.id
      Dealership ID: sgm_dealerships.id
    row: 11
    col: 11
    width: 13
    height: 8
  filters:
  - name: User ID
    title: User ID
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: Incentive_Sales
    explore: sgt_user_vinsales
    listens_to_filters: []
    field: sgt_user_payouts.id
  - name: Dealership ID
    title: Dealership ID
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: Incentive_Sales
    explore: sgt_user_vinsales
    listens_to_filters: []
    field: sgm_dealerships.id
