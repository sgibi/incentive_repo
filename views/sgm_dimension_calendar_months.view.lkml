view: sgm_dimension_calendar_months {
  sql_table_name: dbo.sgm_dimension_calendar_months ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: agent_code {
    type: string
    sql: ${TABLE}."Agent Code" ;;
  }

  dimension: calendar_current_month_code {
    type: number
    sql: ${TABLE}."Calendar Current Month Code" ;;
  }

  dimension: calendar_month_code {
    type: number
    sql: ${TABLE}."Calendar Month Code" ;;
  }

  dimension: calendar_month_name {
    type: string
    sql: ${TABLE}."Calendar Month Name" ;;
  }

  dimension: calendar_month_name_abbreviation {
    type: string
    sql: ${TABLE}."Calendar Month Name Abbreviation" ;;
  }

  dimension: calendar_month_number_of_quarter {
    type: number
    sql: ${TABLE}."Calendar Month Number of Quarter" ;;
  }

  dimension: calendar_month_number_of_year {
    type: number
    sql: ${TABLE}."Calendar Month Number of Year" ;;
  }

  dimension: calendar_previous_month_code {
    type: number
    sql: ${TABLE}."Calendar Previous Month Code" ;;
  }

  dimension: calendar_quarter_code {
    type: string
    sql: ${TABLE}."Calendar Quarter Code" ;;
  }

  dimension: calendar_quarter_name {
    type: string
    sql: ${TABLE}."Calendar Quarter Name" ;;
  }

  dimension: calendar_quarter_number_of_year {
    type: number
    sql: ${TABLE}."Calendar Quarter Number of Year" ;;
  }

  dimension: calendar_type {
    type: string
    sql: ${TABLE}."Calendar Type" ;;
  }

  dimension: calendar_year {
    type: number
    sql: ${TABLE}."Calendar Year" ;;
  }

  dimension_group: calender_first_day_of_month {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."Calender First Day of Month" ;;
  }

  dimension_group: calender_last_day_of_month {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."Calender Last Day of Month" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: month_identifier {
    type: number
    sql: ${TABLE}."Month Identifier" ;;
  }

  dimension_group: report_run {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."Report Run Date" ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      calendar_month_name,
      calendar_quarter_name,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name
    ]
  }
}
