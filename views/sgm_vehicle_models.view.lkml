view: sgm_vehicle_models {
  sql_table_name: dbo.sgm_vehicle_models ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: sg_ac_desc {
    type: string
    sql: ${TABLE}.sg_ac_desc ;;
  }

  dimension: sg_con_ncic {
    type: string
    sql: ${TABLE}.sg_con_ncic ;;
  }

  dimension: sg_dlr_agent {
    type: string
    sql: ${TABLE}.sg_dlr_agent ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name
    ]
  }
}
