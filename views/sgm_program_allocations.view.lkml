view: sgm_program_allocations {
  sql_table_name: dbo.sgm_program_allocations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: allocation_type {
    type: string
    sql: ${TABLE}.allocation_type ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: sgm_dealership_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmDealershipId ;;
  }

  dimension: sgm_program_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmProgramId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_programs.id,
      sgm_dealerships.id,
      sgm_dealerships.dealer_manager_name,
      sgm_dealerships.geography_level_1_name,
      sgm_dealerships.geography_level_2_name,
      sgm_dealerships.geography_level_3_name,
      sgm_dealerships.geography_level_4_name,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name,
      sgt_program_users.count
    ]
  }
}
