view: sgm_periods {
  sql_table_name: dbo.sgm_periods ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: calculation_date_type {
    type: string
    sql: ${TABLE}.calculation_date_type ;;
  }

  dimension: calculation_end {
    type: string
    sql: ${TABLE}.calculation_end ;;
  }

  dimension: calculation_start {
    type: string
    sql: ${TABLE}.calculation_start ;;
  }

  dimension: cancel_reporting_cut_off {
    type: string
    sql: ${TABLE}.cancel_reporting_cut_off ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dealer_approval {
    type: string
    sql: ${TABLE}.dealer_approval ;;
  }

  dimension: financial_approval {
    type: string
    sql: ${TABLE}.financial_approval ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: payment {
    type: string
    sql: ${TABLE}.payment ;;
  }

  dimension: sales_reporting_cut_off {
    type: string
    sql: ${TABLE}.sales_reporting_cut_off ;;
  }

  dimension: sgm_client_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: status {
    type: number
    sql: ${TABLE}.status ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name,
      sgt_dealership_payments.count,
      sgt_dealership_payouts.count,
      sgt_dealership_vol.count,
      sgt_user_payments.count,
      sgt_user_payouts.count,
      sgt_vin_payouts.count,
      sgt_vin_vol.count
    ]
  }
}
