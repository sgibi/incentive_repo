view: sgt_program_payouts {
  sql_table_name: dbo.sgt_program_payouts ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: change_effective_date {
    type: string
    sql: ${TABLE}.changeEffectiveDate ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: end_date {
    type: string
    sql: ${TABLE}.endDate ;;
  }

  dimension: group {
    type: string
    sql: ${TABLE}."group" ;;
  }

  dimension: is_approved {
    type: string
    sql: ${TABLE}.isApproved ;;
  }

  dimension: is_commercial {
    type: string
    sql: ${TABLE}.isCommercial ;;
  }

  dimension: is_kicker {
    type: number
    sql: ${TABLE}.isKicker ;;
  }

  dimension: is_retailed {
    type: string
    sql: ${TABLE}.isRetailed ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: primary_payout_id {
    type: number
    sql: ${TABLE}.primaryPayoutId ;;
  }

  dimension: program_type {
    type: string
    sql: ${TABLE}.program_type ;;
  }

  dimension: range {
    type: string
    sql: ${TABLE}.range ;;
  }

  dimension: region {
    type: string
    sql: ${TABLE}.region ;;
  }

  dimension: sgm_program_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmProgramId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: used_car_definition {
    type: string
    sql: ${TABLE}.usedCarDefinition ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name, sgm_programs.id, sgt_payout_products.count, sgt_payout_tiers.count]
  }
}
