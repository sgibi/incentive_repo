view: sgt_program_users {
  sql_table_name: dbo.sgt_program_users ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dealership_flag {
    type: string
    sql: ${TABLE}.dealership_flag ;;
  }

  dimension: percentage {
    type: number
    sql: ${TABLE}.percentage ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: sgm_dealership_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmDealershipId ;;
  }

  dimension: sgm_program_allocation_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmProgramAllocationId ;;
  }

  dimension: sgm_program_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmProgramId ;;
  }

  dimension: sgm_user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmUserId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  dimension: vin_sale_representative_flag {
    type: string
    sql: ${TABLE}.vin_sale_representative_flag ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      sgm_users.id,
      sgm_users.name,
      sgm_programs.id,
      sgm_dealerships.id,
      sgm_dealerships.dealer_manager_name,
      sgm_dealerships.geography_level_1_name,
      sgm_dealerships.geography_level_2_name,
      sgm_dealerships.geography_level_3_name,
      sgm_dealerships.geography_level_4_name,
      sgm_program_allocations.id,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name
    ]
  }
}
