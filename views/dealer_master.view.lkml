view: dealer_master {
  sql_table_name: SGIIncPortalSrcData_Demo.dbo.DealerMaster ;;

  dimension: current_flag {
    hidden: yes
    type: string
    sql: ${TABLE}."current flag" ;;
  }

  dimension_group: dealer_geography_end {
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."dealer geography end date" ;;
  }

  dimension_group: dealer_geography_start {
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}."dealer geography start date" ;;
  }

  dimension: dealer_manager_name {
    hidden: yes
    type: string
    sql: ${TABLE}."dealer manager name" ;;
  }

  dimension: geography_level_1_code {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 1 code" ;;
  }

  dimension: geography_level_1_manager {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 1 manager" ;;
  }

  dimension: geography_level_1_name {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 1 name" ;;
  }

  dimension: geography_level_1_number {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 1 number" ;;
  }

  dimension: geography_level_1_type_identifier {
    hidden: yes
    type: number
    sql: ${TABLE}."Geography Level 1 Type Identifier" ;;
  }

  dimension: geography_level_2_code {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 2 code" ;;
  }

  dimension: geography_level_2_manager {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 2 manager" ;;
  }

  dimension: geography_level_2_name {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 2 name" ;;
  }

  dimension: geography_level_2_number {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 2 number" ;;
  }

  dimension: geography_level_2_type_identifier {
    hidden: yes
    type: number
    sql: ${TABLE}."Geography Level 2 Type Identifier" ;;
  }

  dimension: geography_level_3_code {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 3 code" ;;
  }

  dimension: geography_level_3_manager {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 3 manager" ;;
  }

  dimension: geography_level_3_name {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 3 name" ;;
  }

  dimension: geography_level_3_number {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 3 number" ;;
  }

  dimension: geography_level_3_type_identifier {
    hidden: yes
    type: number
    sql: ${TABLE}."Geography Level 3 Type Identifier" ;;
  }

  dimension: geography_level_4_code {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 4 code" ;;
  }

  dimension: geography_level_4_manager {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 4 manager" ;;
  }

  dimension: geography_level_4_name {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 4 name" ;;
  }

  dimension: geography_level_4_number {
    hidden: yes
    type: string
    sql: ${TABLE}."geography level 4 number" ;;
  }

  dimension: geography_level_4_type_identifier {
    hidden: yes
    type: number
    sql: ${TABLE}."Geography Level 4 Type Identifier" ;;
  }

  dimension: owner_code {
    hidden: yes
    type: string
    sql: ${TABLE}."owner code" ;;
  }

  dimension: owner_identifier {
    hidden: yes
    type: number
    sql: ${TABLE}."owner identifier" ;;
  }

  dimension: sales_owner_code {
    hidden: yes
    type: string
    sql: ${TABLE}."sales owner code" ;;
  }

  dimension: sg_agt_company {
    hidden: yes
    type: string
    sql: ${TABLE}.sg_agt_company ;;
  }

  dimension: sg_dlr_add1 {
    label: "Dealer Address 1"
    type: string
    sql: ${TABLE}.sg_dlr_add1 ;;
  }

  dimension: sg_dlr_add2 {
    label: "Dealer Address 2"
    type: string
    sql: ${TABLE}.sg_dlr_add2 ;;
  }

  dimension: sg_dlr_agent {
    hidden: yes
    type: string
    sql: ${TABLE}.sg_dlr_agent ;;
  }

  dimension: sg_dlr_city {
    label: "Dealer City"
    type: string
    sql: ${TABLE}.sg_dlr_city ;;
  }

  dimension: sg_dlr_company {
    hidden: yes
    type: string
    sql: ${TABLE}.sg_dlr_company ;;
  }

  dimension: sg_dlr_dealer {
    hidden: yes
    type: string
    sql: ${TABLE}.sg_dlr_dealer ;;
  }

  dimension_group: sg_dlr_edate {
    group_label: "Dealer End"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sg_dlr_edate ;;
  }

  dimension: sg_dlr_fax {
    label: "Fax Number"
    type: string
    sql: ${TABLE}.sg_dlr_fax ;;
  }

  dimension_group: sg_dlr_outofbus {
    label: "Dealer Out Of Business"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sg_dlr_outofbus ;;
  }

  dimension: sg_dlr_phone {
    label: "Dealer Phone Number"
    type: string
    sql: ${TABLE}.sg_dlr_phone ;;
  }

  dimension_group: sg_dlr_sdate {
    group_label: "Dealer Start"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sg_dlr_sdate ;;
  }

  dimension: sg_dlr_state {
    label: "Dealer State"
    type: string
    sql: ${TABLE}.sg_dlr_state ;;
    map_layer_name: us_states
  }

  dimension: sg_dlr_zip {
    label: "Dealer Zip Code"
    map_layer_name: us_zipcode_tabulation_areas
    type: string
    sql: ${TABLE}.sg_dlr_zip ;;
  }

  measure: count {
    type: count
    drill_fields: [dealer_manager_name, geography_level_1_name, geography_level_2_name, geography_level_3_name, geography_level_4_name]
  }
}
