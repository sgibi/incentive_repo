view: sgt_dealership_payouts {
  sql_table_name: dbo.sgt_dealership_payouts ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dealership_payout {
    type: number
    sql: ${TABLE}.dealership_payout ;;
  }

  measure: total_dealerships_payout_amount {
    type: sum
    sql: ${dealership_payout};;
    }


  dimension: is_approved {
    type: string
    sql: ${TABLE}.isApproved ;;
  }

  dimension: is_processed {
    type: number
    sql: ${TABLE}.isProcessed ;;
  }

  dimension: payout_amount {
    type: number
    sql: ${TABLE}.payout_amount ;;
  }

  dimension: percentage {
    type: number
    sql: ${TABLE}.percentage ;;
  }

  dimension: sg_con_dealer {
    type: string
    sql: ${TABLE}.sg_con_dealer ;;
  }

  dimension: sg_con_plc {
    type: string
    sql: ${TABLE}.sg_con_plc ;;
  }

  dimension: sg_con_vin {
    type: string
    sql: ${TABLE}.sg_con_vin ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: sgm_period_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmPeriodId ;;
  }

  dimension: sgm_program_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmProgramId ;;
  }

  dimension: sgt_delearship_payment_id {
    type: number
    sql: ${TABLE}.sgtDelearshipPaymentId ;;
  }

  dimension: sgt_vin_payout_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgtVinPayoutId ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      sgt_user_vinsales.sg_con_dealer,
      sgt_user_vinsales.sg_con_lname,
      sgt_user_vinsales.sg_con_contract,
      sgt_user_vinsales.sg_con_vin,
      sgt_user_vinsales.sg_con_plc,
      sgt_user_vinsales.sg_con_saledate_date,
      sgt_user_vinsales.sg_con4_busdate_date,
      sgt_user_vinsales.sg_con4_rectype,
      sgt_user_vinsales.sg_con_term,
      sgt_vin_payouts.total_payout_amount,
      sgt_user_vinsales.total_user_payout_amount,
      sgt_dealership_payouts.total_dealerships_payout_amount
    ]
  }
}
