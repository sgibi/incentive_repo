view: sgm_product_coverage {
  sql_table_name: dbo.sgm_product_coverage ;;


  dimension: primary_key {
    hidden: yes
    primary_key: yes
    type: string
    sql: ${agent_number} + ${product_code} + ${coverage_code} + ${created_at} ;;
  }



  dimension: agent_number {
    hidden: yes
    type: string
    sql: ${TABLE}.AGENT_NUMBER ;;
  }

  dimension: coverage_code {
    type: string
    sql: ${TABLE}.COVERAGE_CODE ;;
  }

  dimension: coverage_description {
    type: string
    sql: ${TABLE}.COVERAGE_DESCRIPTION ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: mtype {
    type: string
    sql: ${TABLE}.MTYPE ;;
  }

  dimension: product_code {
    type: string
    sql: ${TABLE}.PRODUCT_CODE ;;
  }

  dimension: product_description {
    type: string
    sql: ${TABLE}.PRODUCT_DESCRIPTION ;;
  }

  dimension: rstype {
    type: string
    sql: ${TABLE}.RSTYPE ;;
  }

  dimension: sgm_client_id {
    hidden: yes
    type: number
    sql: ${TABLE}.SGM_CLIENT_ID ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
