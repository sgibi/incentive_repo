view: sgm_payers {
  sql_table_name: dbo.sgm_payers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_name {
    type: string
    sql: ${TABLE}.accountName ;;
  }

  dimension: account_number {
    type: string
    sql: ${TABLE}.accountNumber ;;
  }

  dimension: account_type {
    type: string
    sql: ${TABLE}.accountType ;;
  }

  dimension: auth_token {
    type: string
    sql: ${TABLE}.authToken ;;
  }

  dimension: created_at {
    type: string
    sql: ${TABLE}.createdAt ;;
  }

  dimension: created_by {
    type: string
    sql: ${TABLE}.createdBy ;;
  }

  dimension: dba {
    type: string
    sql: ${TABLE}.dba ;;
  }

  dimension: department {
    type: string
    sql: ${TABLE}.department ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: is_active {
    type: string
    sql: ${TABLE}.isActive ;;
  }

  dimension: legal_name {
    type: string
    sql: ${TABLE}.legalName ;;
  }

  dimension: mailing_address1 {
    type: string
    sql: ${TABLE}.mailingAddress1 ;;
  }

  dimension: mailing_address2 {
    type: string
    sql: ${TABLE}.mailingAddress2 ;;
  }

  dimension: mailing_city {
    type: string
    sql: ${TABLE}.mailingCity ;;
  }

  dimension: mailing_country {
    type: string
    sql: ${TABLE}.mailingCountry ;;
  }

  dimension: mailing_postal_code {
    type: string
    sql: ${TABLE}.mailingPostalCode ;;
  }

  dimension: mailing_state {
    type: string
    sql: ${TABLE}.mailingState ;;
  }

  dimension: p_number {
    type: string
    sql: ${TABLE}.pNumber ;;
  }

  dimension: payer_id {
    type: string
    sql: ${TABLE}.payerId ;;
  }

  dimension: pc_email {
    type: string
    sql: ${TABLE}.pcEmail ;;
  }

  dimension: pc_firstname {
    type: string
    sql: ${TABLE}.pcFirstname ;;
  }

  dimension: pc_lastname {
    type: string
    sql: ${TABLE}.pcLastname ;;
  }

  dimension: pc_mobile {
    type: string
    sql: ${TABLE}.pcMobile ;;
  }

  dimension: pc_title {
    type: string
    sql: ${TABLE}.pcTitle ;;
  }

  dimension: routing_number {
    type: string
    sql: ${TABLE}.routingNumber ;;
  }

  dimension: sales_channel {
    type: string
    sql: ${TABLE}.salesChannel ;;
  }

  dimension: sgm_client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.sgmClientId ;;
  }

  dimension: standard_monthly_fee {
    type: string
    sql: ${TABLE}.standardMonthlyFee ;;
  }

  dimension: tax_id {
    type: string
    sql: ${TABLE}.taxId ;;
  }

  dimension: tax_id_type {
    type: string
    sql: ${TABLE}.taxIdType ;;
  }

  dimension: tiered_monthly_fee {
    type: string
    sql: ${TABLE}.tieredMonthlyFee ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension: updated_at {
    type: string
    sql: ${TABLE}.updatedAt ;;
  }

  dimension: updated_by {
    type: string
    sql: ${TABLE}.updatedBy ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      legal_name,
      pc_firstname,
      pc_lastname,
      account_name,
      sgm_clients.id,
      sgm_clients.client_name,
      sgm_clients.font_h1_name,
      sgm_clients.font_h2_name,
      sgm_clients.font_h3_name
    ]
  }
}
