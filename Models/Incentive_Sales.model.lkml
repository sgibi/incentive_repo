connection: "incentiveuatdb"

include: "/views/*.view.lkml"                # include all views in the views/ folder in this project
include: "/views/*.dashboard"
# include: "/**/view.lkml"                   # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
# explore: order_items {
#   join: orders {
#     relationship: many_to_one
#     sql_on: ${orders.id} = ${order_items.order_id} ;;
#   }
#
#   join: users {
#     relationship: many_to_one
#     sql_on: ${users.id} = ${orders.user_id} ;;
#   }
# }

explore:  sgt_vin_sales {

    join: sgt_vin_claim {
      type: left_outer
      relationship: one_to_one
      sql_on:  ${sgt_vin_sales.vin_sales_id} = ${sgt_vin_claim.vin_sales_id};;
    }

    join: sgm_users {
      type: full_outer
      relationship: many_to_one
      sql_on: ${sgt_vin_claim.sgm_user_id} = ${sgm_users.id} ;;
    }






}


explore: sgt_user_vinsales {
  label: "Individual sales"
#  access_filter: {
#    field: sgm_user_id
#    user_attribute: external_user_id
#  }
  join: dealer_region_market {
    view_label: "Dealer Region Information"
    type: left_outer
    relationship: many_to_one
    sql_on: ${dealer_region_market.dealer_code} = ${sgt_user_vinsales.sg_con_dealer} and ${dealer_region_market.current_flag} = 'y';;
  }

  join: sgm_clients {
    view_label: "Partner Information"
    type: inner
    relationship: many_to_one
    sql_on: ${sgm_clients.id} = ${sgt_user_vinsales.sgm_client_id} ;;
  }
  join: sgm_users {
    view_label: "Users"
    type: left_outer
    relationship: many_to_one
    sql_on: ${sgm_users.id} = ${sgt_user_vinsales.sgm_user_id} ;;
  }

  join: sgm_dealerships {
    view_label: "Dealership Info"
    type: inner
    relationship: many_to_one
    sql_on: ${sgt_user_vinsales.sg_con_dealer} = ${dealer_master.sg_dlr_dealer} and ${sgm_dealerships.current_flag} = 'y' ;;
  }


  join: sgt_user_dealerships {
    view_label: "User Dealerships"
    type: inner
    relationship: many_to_many
    sql_on: ${sgt_user_dealerships.dealership_id} = ${sgm_dealerships.id}  ;;
  }

  join: sgt_vin_payouts {
    view_label: "Payouts"
    type: left_outer
    relationship: one_to_many
    sql_on: ${sgt_user_vinsales.sg_con_vin} = ${sgt_vin_payouts.VIN} and ${sgt_user_vinsales.sg_con_plc} = ${sgt_vin_payouts.product_code} and cast(${sgt_user_vinsales.sg_con_dealer} as varchar) = cast(${sgt_vin_payouts.sgm_dealer_id} as varchar);;
    }


  join: sgt_user_payouts {
    view_label: "User Payouts"
    type: left_outer
    relationship: one_to_many
    sql_on:  ${sgt_vin_payouts.id} = ${sgt_user_payouts.sgt_vin_payout_id};;
  }

  join: sgt_dealership_payouts {
    view_label: "Dealership Payouts"
    type: left_outer
    relationship: one_to_many
    sql_on: ${sgt_vin_payouts.id} = ${sgt_dealership_payouts.sgt_vin_payout_id} ;;
  }

  join: sgt_user_roles {
    view_label: "User To Role"
    type: left_outer
    relationship: many_to_one
    sql_on: ${sgm_users.id} = ${sgt_user_roles.user_id} ;;
  }

  join: sgm_roles {
    view_label: "Roles"
    type: left_outer
    relationship: many_to_one
    sql_on: ${sgm_roles.id} = ${sgt_user_roles.role_id} ;;
  }

  join: sgm_periods {
    view_label: "Incentive Periods"
    type: left_outer
    relationship: many_to_many
    sql_on: case when ${sgm_periods.calculation_date_type} = 'Business Date' then ${sgt_user_vinsales.sg_con4_busdate_date}
                 when ${sgm_periods.calculation_date_type} = 'Sales Date' then ${sgt_user_vinsales.sg_con_saledate_date}
                else null end between ${sgm_periods.calculation_start} and ${sgm_periods.calculation_end};;
  }

  join: sgm_programs {
    view_label: "Incentive Programs"
    type: inner
    relationship: many_to_one
    sql_on: ${sgm_programs.id} = ${sgt_vin_payouts.sgm_program_id} ;;
  }

  join: dealer_master {
    view_label: "Dealer Information"
    type: inner
    relationship: many_to_one
    sql_on: ${dealer_master.sg_dlr_dealer} = ${sgt_user_vinsales.sg_con_dealer} ;;
  }
}
