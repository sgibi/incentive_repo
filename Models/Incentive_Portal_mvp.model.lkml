connection: "incentiveuatdb"

include: "/views/*.view.lkml"                # include all views in the views/ folder in this project
# include: "/**/view.lkml"                   # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
# explore: order_items {
#   join: orders {
#     relationship: many_to_one
#     sql_on: ${orders.id} = ${order_items.order_id} ;;
#   }
#
#   join: users {
#     relationship: many_to_one
#     sql_on: ${users.id} = ${orders.user_id} ;;
#   }
# }

explore: sgt_vin_sales {
  label: "Incentive Portal Sales"

  join: sgm_clients {
    view_label: "Client Information"
    type: inner
    relationship: many_to_one
    sql_on: ${sgt_vin_sales.sgm_client_id} = ${sgm_clients.id} ;;
  }

  join: sgt_vin_claim {
    relationship: one_to_one
    type: left_outer
    sql_on: ${sgt_vin_claim.vin_sales_id} = ${sgt_vin_sales.vin_sales_id} ;;
  }

  join: sgm_users {
    view_label: "User Information"
    relationship: many_to_one
    type: left_outer
    sql_on: ${sgm_users.id} = ${sgt_vin_claim.sgm_user_id} ;;
  }

  join: sgm_dealerships {
    view_label: "Dealer Information"
    relationship: many_to_one
    type: inner
    sql_on: ${sgt_vin_sales.dealer_number} = ${sgm_dealerships.sg_dlr_dealer} ;;
  }

  join: sgm_product_coverage {
    view_label: "Product Coverage Information"
    relationship: many_to_one
    type: inner
    sql_on: ${sgm_product_coverage.product_code} = ${sgt_vin_sales.product_code} and
            ${sgm_product_coverage.coverage_code} = ${sgt_vin_sales.product_coverage} and
            ${sgm_product_coverage.agent_number} = ${sgt_vin_sales.agent_number};;
  }

  join: sgt_vin_payments_detail {
    view_label: "VIN Payouts"
    relationship: one_to_many
    type: left_outer
    sql_on:  case when ${sgt_vin_payments_detail.dealership_payee} = 1 then ${sgt_vin_payments_detail.sgm_dealer_id}
                  else ${sgt_vin_payments_detail.sgm_user_id} end =
             case when ${sgt_vin_payments_detail.dealership_payee} = 1 then ${sgm_dealerships.id}
                  else ${sgt_vin_claim.sgm_user_id} end and
            ${sgt_vin_sales.vin} = ${sgt_vin_payments_detail.vin} and
            ${sgt_vin_sales.product_code} = ${sgt_vin_payments_detail.product_code} and
            ${sgt_vin_sales.product_coverage} = ${sgt_vin_payments_detail.coverage_code}
            ;;

    }


  }

  explore: sgt_vin_payments_summary {
    label: "Payment Summary Explore"

    join: sgm_dealerships {
      view_label: "Dealership Information"
      relationship: many_to_one
      sql_on: ${sgm_dealerships.id} = ${sgt_vin_payments_summary.sgm_dealer_id} ;;
    }

    join: sgm_users {
      view_label: "User Information"
      relationship: many_to_one
      sql_on: ${sgm_users.id} = ${sgt_vin_payments_summary.sgm_user_id} ;;
    }
  }
